
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <MQ135.h>
#include "Adafruit_SHT31.h"

int ppm = -1;
float temperature = -1;
float humidity = -1;

String licence = "AQ-123123";

const char* ssid = "quarks";
const char* password = "1104maja";
String url = "http://3fvadlwujvaa4n6rwagd5owrfiy5yxj5rhpnyjcpav63c3ljgula.remote.moe:8080/api/data";

void setup() {
  Serial.begin(115200);
  Serial.println("=======================SETUP=======================");
  startWifi();
  initializeSHT31();
  Serial.println("=======================SETUP DONE=======================");
}

void loop() {

  ppm = mq135PPM();
  Serial.print("PPM cestice: ");
  Serial.println(ppm);

  temperature = sht31Temperature();
  humidity = sht31Humidity();
  Serial.print("Temperatura: ");
  Serial.println(temperature);
  Serial.print("Vlaznost ");
  Serial.print(humidity);
  Serial.println("%");

  sendDataToServer(ppm, temperature, humidity);

  delay(20000);
}
