Adafruit_SHT31 sht31;

void initializeSHT31(){
  sht31 = Adafruit_SHT31();
  Serial.println("SHT31 test");
  if(! sht31.begin(0x44)){
    Serial.println("Could not find SHT31");
  }
}

float sht31Temperature(){
  return sht31.readTemperature();
}

float sht31Humidity(){
  return sht31.readHumidity();
}
