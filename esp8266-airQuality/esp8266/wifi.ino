void startWifi(){
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi"); 
}

void sendDataToServer(int ppm, float temperature, float humidity){
  Serial.println("=======================SENDING DATA TO SERVER=======================");
  String body = createBody(ppm, temperature, humidity);
  Serial.println(body);
  sendPostHTTPRequest(body);
}

String createBody(int ppm, float temperature, float humidity){
  String body = "{\"licence\":\"";
  body = body+licence;
  body = body+"\",\"airQuality\":";
  body = body+ppm;
  body = body+",\"temperature\":";
  body = body+temperature;
  body = body+",\"humidity\":";
  body = body+humidity;
  body = body+"}";

  return body;  
}

void sendPostHTTPRequest(String body){
  WiFiClient client;
  HTTPClient http;
  http.begin(client, url);
  Serial.println(url);
  http.addHeader("Content-Type", "application/json");
  int httpResponseCode = http.POST(body);
  if (httpResponseCode > 0) {
    Serial.print("HTTP POST request sent successfully. Response code: ");
    Serial.println(httpResponseCode);
  } else {
    Serial.print("Error sending HTTP POST request. Error code: ");
    Serial.println(httpResponseCode);
  }
  http.end();
}
