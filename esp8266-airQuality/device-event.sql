create table device_event
(
    licence     varchar(255) null,
    air_quality int          null,
    temperature int          null,
    humidity    int          null,
    id          int auto_increment
        primary key
);
