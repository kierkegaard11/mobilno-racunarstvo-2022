from flask import Flask, request
import mysql.connector

app = Flask(__name__)

mysql_host = 'localhost'
mysql_port = 3306
mysql_user = 'user'
mysql_password = 'pass'
mysql_database = 'test'
mysql_table = 'device_event'

@app.route('/api/data', methods=['POST'])
def parse_json():
    data = request.get_json()

    if not data:
        return "Invalid JSON", 400

    licence = data.get('licence')
    air_quality = data.get('airQuality')
    temperature = data.get('temperature')
    humidity = data.get('humidity')

    # Process the received data as required
    # ...

    db_connection = mysql.connector.connect(
        host=mysql_host,
        port=mysql_port,
        user=mysql_user,
        password=mysql_password,
        database=mysql_database
    )

    cursor = db_connection.cursor()

    query = "INSERT INTO {} (licence, temperature, air_quality, humidity) VALUES (%s, %s, %s, %s)".format(mysql_table)
    values = (licence, temperature, air_quality, humidity)
    cursor.execute(query, values)
    
    db_connection.commit()
    
    cursor.close()
    db_connection.close()

    return "Data received, parsed, and inserted successfully"


if __name__ == '__main__':
    app.run()
