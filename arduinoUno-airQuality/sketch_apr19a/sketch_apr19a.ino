
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <MQ135.h>
#include "Adafruit_SHT31.h"


int ppm = -1;
float temperature = -1;
float humidity = -1;
int val = 0;

MQ135 gasSensor = MQ135(0);
Adafruit_SHT31 sht31 = Adafruit_SHT31();

int redPin = 11;
int greenPin = 10;
int bluePin = 8;

int pirPin = 7;

void setup() {
  
  delay(700);
  Serial.begin(9600);
  delay(700);

  //pinovi za RGB diodu
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  //pin za PIR senzor
  pinMode(pirPin, INPUT);
  
  Serial.println("SHT31 test");
  if (! sht31.begin(0x44)) {
  Serial.println("Couldn't find SHT31");
  }
}

void loop() {

  //citanje vrednosti PIR senzora; ako je voltaza na digitalnom pinu visoka, pokret je detektovan
  //RGB dioda ce zasvetleti plavo
  val = digitalRead(pirPin);
  if(val == HIGH){
  analogWrite(redPin,0);
  analogWrite(greenPin,0);
  analogWrite(bluePin,255);
  Serial.println("Motion detected!");
  Serial.println("");
  delay(2000);
  }

  
  analogWrite(redPin,0);
  analogWrite(greenPin,255);
  analogWrite(bluePin,0);

  
  //citanje vrednosti senzora za kvalitet vazduha
  ppm = gasSensor.getPPM();

  //ako je detektovana vrednost manja od 400, vazduh je cist i dioda svetli zeleno
  if(ppm<400){
  analogWrite(redPin,0);
  analogWrite(greenPin,255);
  analogWrite(bluePin,0);
  }

  //ako je detektovana vrednost izmedju 400 i 800, detektovani su stetni gasovi, dioda svetli narandzasto
  if(ppm>=400 && ppm<800){
  analogWrite(redPin,255);
  analogWrite(greenPin,128);
  analogWrite(bluePin,0); 
  }

  //ako je detektovana vrednost veca ili jednaka 800, detektovani su stetni gasovi u vecoj kolicini, dioda svetli crveno
  if(ppm>=800){
  analogWrite(redPin,255);
  analogWrite(greenPin,0);
  analogWrite(bluePin,0); 
  }
  
  Serial.println("");
  Serial.print("PPM particles: ");
  Serial.println(ppm);

  //ocitavanje temperature i vlaznosti
  temperature = sht31.readTemperature();
  humidity = sht31.readHumidity();

  Serial.print("Temperature in C: ");
  Serial.println(temperature);
  Serial.print("Humidity in %: ");
  Serial.println(humidity); 

}
